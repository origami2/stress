#!/usr/bin/env node

var Mocha = require('mocha'),
    fs = require('fs'),
    async = require('async');
    
function getTestsMarkedAsOnly(suite) {
  var onlyTests = [];
  
  suite
  .eachTest(function (test) {
    if (test.parent._onlyTests.length &&
        test.parent._onlyTests.indexOf(test) > -1) onlyTests.push(test);
  });
  
  return onlyTests;
}

function getTests(suite) {
  var onlyTests = getTestsMarkedAsOnly(suite);
  
  if (onlyTests.length) return onlyTests;
  
  var tests = [];
  
  suite.eachTest(function (test) {
    tests.push(test);
  });
  
  return tests;
}

// Instantiate a Mocha instance.
var mocha = new Mocha();

var argv = require('minimist')(process.argv.slice(2));

if (argv.v) {
  console.log(
    'origami-stress version ' + 
    JSON.parse(
      require('fs')
      .readFileSync(
        require('path')
        .join(
          __dirname,
          '..',
          'package.json'
        ) 
      )
    )
    .version
  );
  process.exit();
}

var options = {
  timeout: argv.t || 5000,
  silent: argv.s,
  noErrors: argv.e,
  json: argv.j,
  onlyCheck: argv.c
};

async
.eachSeries(
  argv._,
  function (path, callback) {
    path = require('path').resolve(path);
    
      // Add each .js file to the mocha instance
    fs
    .readdirSync(path)
    .filter(
      function(file){
          // Only keep the .js files
          return file.substr(-3) === '.js';
      }
    )
    .forEach(
      function(file){
        mocha.addFile(
          require('path').join(path, file)
        );
      }
    );
    
    callback();
  },
  function (err) {
    if (err) {
      console.error(err);
      return process.exit(-1);
    }
    
    mocha
    .loadFiles(function () {
      var tests = getTests(mocha.suite).filter(function (test) {
        return !test.isPending();
      });
      
      if (options.onlyCheck) {
        return require('../onlyCheck')(tests, options);
      } else {
        return require('../runSequence')(tests, options);
      }
    });  
  }
);
