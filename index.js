var Mocha = require('mocha');

module.exports = {
  testRunner: function (test) {
    return function (done) {
      var parent = test.parent;
      var suite = new Mocha.Suite('stress', new Mocha.Context());
      
      while(parent) {
        var hook;
        for (var i = 0; i < parent._beforeAll.length; i++) {
           hook = parent._beforeAll[i];
          
           suite.beforeEach(hook.title, hook.fn);
        }
        
        for (var i = 0; i < parent._beforeEach.length; i++) {
          hook = parent._beforeEach[i];
          
          suite.beforeEach(hook.title, hook.fn);
        }
        
        for (var i = 0; i < parent._afterAll.length; i++) {
          hook = parent._afterAll[i];
          
          suite.afterEach(hook.title, hook.fn);
        }
        
        for (var i = 0; i < parent._afterEach.length; i++) {
          hook = parent._afterEach[i];
          
          suite.afterEach(hook.title, hook.fn);
        }
        
        parent = parent.parent;
      }
      
      var tempTest = new Mocha.Test(test.title, test.fn);
      suite.addTest(tempTest);
      
      var runner = new Mocha.Runner(suite);
    
      runner
      .runTests(
        suite,
        function () {
          if (tempTest.state === 'failed') {
            done(tempTest._trace);
          } else {
            done();
          }
        }
      );
    };
  },
  count: function (implementation, time) {
    return new Promise(function (resolve, reject) {
      var counter = 0;
      var errors = 0;

      var start = Date.now();
      var endAt = start + time;
      
      var runAgain = function () {
        if (endAt > Date.now()) {
          implementation(function(err) {
            if (err) errors += 1;
            counter++;
            process.nextTick(runAgain);
          });
        }
        else {
          resolve({
            times: counter,
            errors: errors
          });
        }
      };
      
      process.nextTick(runAgain);
    });
  }
};