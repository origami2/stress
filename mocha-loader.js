var NodeVM = require('vm2').NodeVM;
var Suite = require('./suite');
var Test = require('./test');
var shortid = require('shortid');

function Aggregator(file) {
  this.file = file;
  this.tests = [];
  this.currentPath = [{
    id: 'root',
    title: file,
    suites: [],
    hooks: {
      beforeEach: [],
      afterEach: [],
      before: [],
      after: []
    }
  }];
}

Aggregator.prototype.makeDescribe = function () {
  var self = this;
  
  return function (title, fn) {
    self.currentPath.push({
      id: shortid.generate(),
      parent: self.currentPath.length ? self.currentPath.slice(-1)[0].id : 'root',
      title: title,
      hooks: {
        beforeEach: [],
        afterEach: [],
        before: [],
        after: []
      },
      tests: [],
      suites: []
    });
    
    fn();
    
    self.currentPath.pop();
  };
};

Aggregator.prototype.makeContext = function () {
  var self = this;
  
  return function (title, fn) {
    self.currentPath.push({
      id: shortid.generate(),
      parent: self.currentPath.length ? self.currentPath.slice(-1)[0].id : 'root',
      title: title,
      context: true,
      suites: [],
      hooks: {
        beforeEach: [],
        afterEach: [],
        before: [],
        after: []
      },
      tests: []
    });
    
    fn();
    
    self.currentPath.pop();
  };
};

Aggregator.prototype.makeIt = function () {
  var self = this;
  
  var it = function (title, fn) {
    self
    .tests
    .push({
      id: shortid.generate(),
      parent: self.currentPath.length ? self.currentPath.slice(-1)[0].id : 'root',
      title: title,
      path: [].concat(self.currentPath),
      fn: fn
    });
  };
  
  it.skip = function (title, fn) {
    self
    .tests
    .push({
      id: shortid.generate(),
      parent: self.currentPath.length ? self.currentPath.slice(-1)[0].id : 'root',
      title: title,
      path: [].concat(self.currentPath),
      skip: true,
      fn: fn
    });
  };
  
  return it;
};

Aggregator.prototype.makeBeforeEach = function () {
  var self = this;
  
  return function (fn) {
    self.currentPath.slice(-1)[0].hooks.beforeEach.push({
      id: shortid.generate(),
      parent: self.currentPath.slice(-1)[0].id,
      kind: 'beforeEach',
      fn: fn
    });
  };
};
function MochaLoader() {
}

MochaLoader.prototype.loadFile = function (path) {
  if (!path) throw new Error('path is required');
  
  return new Promise(function (resolve, reject) {
    try {
      var aggregator = new Aggregator(path);

      var vm = new NodeVM({
        console: 'inherit',
        sandbox: {
          describe: aggregator.makeDescribe(),
          it: aggregator.makeIt(),
          beforeEach: aggregator.makeBeforeEach(),
          context: aggregator.makeContext()
        },
        require: {
            external: true,
            builtin: [
              'fs',
              'path',
              'assert',
              'events'
            ]
        }
      });
      
      vm.run(require('fs').readFileSync(path, 'utf8'), path);

      var tests = aggregator.tests;
      
      for (var i = 0; i < tests.length; i++) {
        tests[i].parents = tests[i].path.slice().reverse();
      }
      
      var rootSuite = new Suite(path);
      
      if (!tests.length) return resolve(suite);
      
      var byId = {
        'root': rootSuite
      };
      
      for (var i = 0; i < tests.length; i++) {
        var test = tests[i];
      
        var newTest = byId[test.id] = new Test(test.fn);
        
        for (var j = 0; j < test.parents.length; j++) {
          var rawSuite = test.parents[j];
          
          if (!rawSuite.parent) {
            continue;
          }
          
          var newSuite = new Suite(rawSuite.title);
          newSuite.addTest(test.title, newTest);
          
          byId[rawSuite.parent].addSuite(newSuite);
        }
      }

      resolve(rootSuite);
    } catch (e) {
      reject(e);
    }
  });
};

module.exports = MochaLoader;