module.exports = function (tests, options) {
  var async = require('async');
  
  async
  .eachSeries(
    tests,
    function (test, callback) {
      require('.').testRunner(test)(function (err) {
        if (err) {
          var fullTitle = test.parent.fullTitle() + ' ' + test.title;
    
          console.log(fullTitle + ' failed');
        }
        
        callback();
      });
    },
    function () {
      process.exit();
    }
  );
};