module.exports = function (tests, options) {
  var silent = options.silent;
  var ne = options.noErrors;
  var timeout = options.timeout;
  var json = options.json;
  var stats = {};
  
  var async = require('async');
  var stress = require('.');
  
  async
  .eachSeries(
    tests,
    function (test, callback) {
      var runTest = require('.').testRunner(test);
      
      if (!silent) {
        console
        .log(
          'starting ' +
          test.parent.fullTitle() + ' ' + test.title
        );
      }
      
      var fullTitle = test.parent.fullTitle() + ' ' + test.title;
      
      stress
      .count(
        function (done) {
          runTest(function (err) {
            if (err && !silent && !ne) {
              console.error('test ' + fullTitle + ' failed:');
              console.error(err);
            }
            
            done(err);
          });
        },
        timeout
      )
      .then(
        function (counterStats) {
          var parent = test.parent;
          var path  = [];
          
          while(parent && parent.title) {
            path.push(parent.title);
            
            parent = parent.parent;
          }
          
          var parentStats = stats;
          
          for (var i = 0; i < path.length; i ++) {
            parentStats[path[i]] = parentStats[path[i]] || {};
            
            parentStats = parentStats[path[i]];
          }
          
          var testStats = parentStats[test.title] = JSON.parse(JSON.stringify(counterStats));
          
          if (!silent) {
            console
            .log(
              'finished ' +
              fullTitle +
              ' which ran ' +
              testStats.times + 
              ' times in ' +
              timeout + 
              ' ms with an average time of ' +
              (timeout / testStats.times).toFixed(2) + ' ms per run' +
              (testStats.errors ? ' and it had ' + testStats.errors + ' failed runs' : '')
            );
          }
          
          callback();
        }
      )
      .catch(callback);
    },
    function (err) {
      if (err) {
        console.error(err);
        process.exit(-1);
      }
      
      if (json) {
        var asJson = JSON.stringify(
          {
            timeout: timeout,
            tests: stats
          }, 
          null, 
          2
        );
        
        if (json === true) {
          console.log(asJson);
        } else if (typeof(json) === 'string') {
          require('fs')
          .writeFileSync(
            json,
            asJson,
            'utf8'
          );
        }
      }
      
      process.exit();
    }
  );
};