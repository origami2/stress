var EventEmitter = require('events').EventEmitter;

function Runner() {
  EventEmitter.call(this);
}

Runner.prototype.run = function (metadata, runnable) {
  var self = this;
  
  return new Promise(function (resolve, reject) {
    self
    .emit('start-run', metadata, runnable);
    
    runnable
    .run(self)
    .then(function (result) {
      self.emit('stop-run', metadata, runnable);
      self.emit('run-ok', metadata, runnable);
      
      resolve(result);
    })
    .catch(function (err) {
      self.emit('stop-run', metadata, runnable);
      
      if (metadata.kind === 'test') {
        var newMetadata = {};
        for(var k in metadata) newMetadata[k] = metadata[k];
        newMetadata.error = err;
        
        self.emit('run-fail', newMetadata, err, runnable);
        
        reject(err);
      } else resolve();
    });
  });
};

require('util').inherits(Runner, EventEmitter);

module.exports = Runner;