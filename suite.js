var async = require('async');

function Suite(title) {
 if (!title) throw new Error('title is required');
 
 this.title = title;
 this.tests = [];
 this.suites = [];
}

Suite.prototype.addTest = function (title, test) {
 if (!title) throw new Error('title is required');
 if (!test) throw new Error('test is required');
 
 this.tests.push({
   title: title,
   test: test
 });
};

Suite.prototype.addSuite = function (suite) {
  if (!suite) throw new Error('suite is required');
  
  if (this.suites.indexOf(suite) !== -1) return;
  this.suites.push(suite);
};

Suite.prototype.run = function (runner) {
  if (!runner) throw new Error('runner is required');
  
  var self = this;
  
  return new Promise(function (resolve, reject) {
    var toRun = []
      .concat(
        self.suites.map(function(s) {
          return {
            title: s.title,
            kind: 'suite',
            runnable: s
          };
        }),
        self.tests.map(
          function (t) { 
            return {
              runnable: t.test,
              kind: 'test',
              title: t.title
            };
          }
        )
      );
      
    async
    .eachSeries(
      toRun,
      function (item, callback) {
        runner
        .run({
          kind: item.kind,
          title: item.title
        }, item.runnable)
        .catch(function () {
          callback();
        })
        .then(function (){
          callback();
        });
      },
      function (err) {
        if (err) return reject(err);
        
        resolve();
      }
    )
  });
}

module.exports = Suite;