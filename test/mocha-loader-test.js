var assert = require('assert');
var MochaLoader = require('../mocha-loader');
var Runner = require('../runner');

describe('MochaLoader', function () {
  describe('.loadTest', function () {
    var target;
    
    beforeEach(function () {
      target = new MochaLoader();
    });
    
    it('requires a path', function () {
      assert.throws(
        function () {
          target.loadFile();
        },
        /path is required/
      );
    });
    
    it('loads test', function (done) {
      target
      .loadFile(require('path').join(__dirname, 'sample-tests', 'test1.js'))
      .catch(done)
      .then(function (suite) {
        try {
          assert(suite);
          assert.equal(suite.suites.length, 1);
          assert.equal('d1', suite.suites[0].title);
          assert.equal(suite.suites[0].tests.length, 1);
          assert.equal('t1', suite.suites[0].tests[0].title);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('runs test', function (done) {
      var runner = new Runner();
      
      var ran = [];

      runner
      .on('start-run', function (metadata) {
        ran.push(metadata);
      });
        

      target
      .loadFile(require('path').join(__dirname, 'sample-tests', 'test1.js'))
      .catch(done)
      .then(function (suite) {
        suite
        .run(runner)
        .then(function () {
          try {
            assert.deepEqual(
              ran, 
              [
                { kind: 'suite', title: 'd1' },
                { kind: 'test', title: 't1' }
              ]);
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    });
    
    it('runs failing tests', function (done) {
      var runner = new Runner();
      
      var ran = [];

      runner
      .on('run-fail', function (metadata) {
        try {
          assert(metadata.error);
          
          done();
        } catch (e) {
          done(e);
        }
      });
        
      target
      .loadFile(require('path').join(__dirname, 'sample-tests', 'test2.js'))
      .catch(done)
      .then(function (suite) {
        suite
        .run(runner);
      });
    });
    
    it.only('runs both sync and async tests', function (done) {
      var runner = new Runner();
      
      var ran = [];

      runner
      .on('run-ok', function (metadata) {
        if (metadata.kind !== 'test') return;
        ran.push(['ok',metadata]);
      });

      runner
      .on('run-fail', function (metadata) {
        if (metadata.kind !== 'test') return;
        ran.push(['fail', metadata]);
      });
        
      target
      .loadFile(require('path').join(__dirname, 'sample-tests', 'test3.js'))
      .catch(done)
      .then(function (suite) {
        suite
        .run(runner)
        .then(function () {
          try {
            assert.equal(ran.length, 4);
            assert.equal(ran[0][0], 'ok');
            assert.equal(ran[1][0], 'fail');
            assert.equal(ran[2][0], 'ok');
            assert.equal(ran[3][0], 'fail');
            assert.equal(ran[0][1].title, 't3');
            assert.equal(ran[1][1].title, 't4');
            assert.equal(ran[2][1].title, 't5');
            assert.equal(ran[3][1].title, 't6');
            assert(ran[1][1].error);
            assert(ran[1][1].error instanceof Error);
            assert.equal(ran[1][1].error.message, 'oops');
            assert(ran[3][1].error);
            assert.equal(ran[3][1].error, 'oops');
          
            done();
          } catch (e) {
            done(e);
          }
        })
      });
    });
  })
});