describe('Runner', function () {
  var Runner = require('../runner');
  var Test = require('../test');
  var assert = require('assert');
  
  describe('.run',function () {
    it('emits start-test with metadata', function (done) {
      var runner = new Runner();
      
      runner.on('start-run', function (metadata, test) {
        try {
          assert.deepEqual(
            metadata,
            {someData: true}
          );
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      runner
      .run(
        {someData: true},
        {
          run: function () {
            return Promise.resolve();
          }
        }
      );
    });

    it('emits stop-test when test is done', function (done) {
      var runner = new Runner();
      var ran = false;
      
      runner
      .on('stop-run', function (metadata, test) {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      runner
      .run(
        {someData: true},
        {
          run: function () {
            ran = true;
            
            return Promise.resolve();
          }
        }
      );
    });

    it('emits stop-run when test is failed', function (done) {
      var runner = new Runner();
      var ran = false;
      
      runner
      .on('stop-run', function (metadata, test) {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      runner
      .run(
        {someData: true},
        {
          run: function () {
            ran = true;
            
            return Promise.reject();
          }
        }
      );
    });

    it('emits run-ok when test is done', function (done) {
      var runner = new Runner();
      var ran = false;
      
      runner
      .on('run-ok', function (metadata, test) {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      runner
      .run(
        {someData: true},
        {
          run: function () {
            ran = true;
            
            return Promise.resolve();
          }
        }
      );
    });

    it('emits run-fail when test is failed', function (done) {
      var runner = new Runner();
      var ran = false;
      var targetTest = new Test(function () {
        ran = true;
        
        throw new Error('my error');
      });
      
      runner
      .on('run-fail', function (metadata, err, test) {
        try {
          assert(ran);
          assert(metadata);
          assert.equal(metadata.kind, 'test');
          assert(metadata.error);
          assert(metadata.error instanceof Error);
          assert.equal(metadata.error.message, 'my error');
          assert.equal(err.message, 'my error');
          assert.equal(test, targetTest);
          
          done();
        } catch (e) {
          done(e);
        }
      });
      
      runner
      .run(
        {kind: 'test'},
        targetTest
      );
    });
  });
});