var Suite = require('../suite');
var assert = require('assert');

describe('Suite', function () {
  it('requires title', function () {
    assert.throws(
      function () {
        new Suite();
      },
      /title is required/
    );
  });
  
  describe('.addTest', function () {
    it('requires title', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.addTest();
        },
        /title is required/
      );
    });
    
    it('requires test', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.addTest('test1');
        },
        /test is required/
      );
    });
  });
  
  describe('.addSuite', function () {
    it('requires suite', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.addSuite();
        },
        /suite is required/
      );
    });
    
    it('accepts suite', function () {
      var target = new Suite('title');
    
      target.addSuite({});
    });
    
    it('do not adds suite twice', function () {
      var target = new Suite('title');
    
      var child = {};
      
      target.addSuite(child);
      target.addSuite(child);
      
      assert.equal(1, target.suites.length)
    });
  });
  
  describe('.run', function () {
    it('requires runner', function () {
      assert.throws(
        function () {
          var target = new Suite('title');
        
          target.run();
        },
        /runner is required/
      );
    });
    
    it('runs tests', function (done) {
      var target = new Suite('title');
      var ran = false;
    
      target
      .addTest(
        'test1',
        {
          run: function () {
            ran = true;
            
            return Promise.resolve();
          }
        }
      );
      
      var runner = {
        run: function (metadata, runnable) {
          return runnable.run();
        }
      };
      
      target
      .run(runner)
      .then(function () {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('runs suite', function (done) {
      var target = new Suite('title');
      var ran = false;
    
      target
      .addSuite(
        {
          run: function () {
            ran = true;
            
            return Promise.resolve();
          }
        }
      );
      
      var runner = {
        run: function (metadata, runnable) {
          return runnable.run();
        }
      };
      target.run(runner)
      .then(function () {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
  });
});