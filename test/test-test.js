describe('Test', function () {
  var Test = require('../test');
  var assert = require('assert');
  
  it('requires implementation', function () {
    assert.throws(
      function () {
        new Test();
      },
      /implementation is required/
    );
  });
  
  it('accepts implementation', function () {
    new Test(function () {});
  });
  
  describe('.run',function () {
    it('runs implementation', function (done) {
      var ran = false;
      
      var test = new Test(function () {
        ran = true;
      });
      
      test
      .run()
      .then(function () {
        try {
          assert(ran);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    context('with pre-run', function () {
      it('runs pre-run', function (done) {
        var ran = [];
        
        var test = new Test(
          function () {
            ran.push('test');
          }
        );
        
        test.addPreRun({
          run: function () {
            ran.push('pre-run');
            
            return Promise.resolve();
          }
        });
        
        test
        .run()
        .then(function () {
          try {
            assert.deepEqual(
              ran,
              ['pre-run','test']
            );
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    });
    
    context('with post-run', function () {
      it('runs post-run', function (done) {
        var ran = [];
        
        var test = new Test(function () {
          ran.push('test');
        });
        
        test.addPostRun({
          run: function () {
            ran.push('post-run');
            
            return Promise.resolve();
          }
        });
        
        test
        .run()
        .then(function () {
          try {
            assert.deepEqual(
              ran,
              ['test', 'post-run']
            );
            
            done();
          } catch (e) {
            done(e);
          }
        });
      });
    })
  });
  
  describe('.addPreRun',function () {
    it('requires implementation', function () {
      var test = new Test(function () {});
      
      assert.throws(
        function() {
          test.addPreRun();
        },
        /runnable is required/
      );
    });
    
    it('accepts implementation', function () {
      var test = new Test(function () {});
      
      test.addPreRun({});
    });
  });
  
  describe('.addPostRun',function () {
    it('requires implementation', function () {
      var test = new Test(function () {});
      
      assert.throws(
        function() {
          test.addPostRun();
        },
        /runnable is required/
      );
    });
    
    it('accepts implementation', function () {
      var test = new Test(function () {});
      
      test.addPostRun({});
    });
  });
});