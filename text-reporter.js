function TextReporter(runner) {
  runner.on('run-ok', function (metadata) {
    if (metadata.kind !== 'test') return;
    console.log(metadata.title + ' ran ok');
  });
  runner.on('run-fail', function (metadata) {
    if (metadata.kind !== 'test') return;
    console.log(metadata.title + ' ran failed');
  });
}

module.exports = TextReporter;